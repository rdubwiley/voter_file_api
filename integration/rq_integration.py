import string
import random
from time import sleep

from vf_api.extensions import db
from vf_api.models import Task, User, VoterList
from vf_api.tasks.voter_list import run_voter_list_query   


def id_generator(size=20, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


def test_rq_integration():
    user = db.session.query(User).filter_by(username='admin').first()
    if not user:
        user = User(
            username = 'testing999',
            email = 'admin@mail.com',
            password = id_generator()
        )
        db.session.add(user)
        db.session.commit()
        user = db.session.query(User).filter_by(username='testing999').first()
    vl = db.session.query(VoterList).filter_by(name='Test').first()
    if not vl:
        vl = VoterList(
            user_id = user.id,
            name = 'Test',
            query = 'SELECT * FROM voter_file.voters LIMIT 5',
            status = 'New'
        )
        db.session.add(vl)
        db.session.commit()
        vl = db.session.query(VoterList).filter_by(name='Test').first()
    result = run_voter_list_query.queue(vl.user_id, vl.id)
    for i in range(10):
        if not result.result:
            sleep(1)
    user_id = user.id
    del user
    del vl
    user = db.session.query(User).filter_by(id=user_id).first()
    db.session.close()
    vl = db.session.query(VoterList).filter_by(name='Test').first()
    try:
        if len(vl.voter_list) > 0 and len(vl.voter_ids) == 5:
            print("RQ integration test succeeded")
        else:
            print("RQ integration test failed for programming reasons")
    except Exception:
        print("RQ integration test failed for session reasons")
    db.session.delete(vl)
    db.session.commit()
    tasks = db.session.query(Task).filter_by(user_id=user.id).all()
    for task in tasks:
        db.session.delete(task)
    db.session.commit()
    db.session.delete(user)  
    db.session.commit()