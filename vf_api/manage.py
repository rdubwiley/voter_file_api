import click
from flask.cli import FlaskGroup

from vf_api.app import create_app
from integration import integration_tests


def create_vf_api(info):
    return create_app(cli=True)


@click.group(cls=FlaskGroup, create_app=create_vf_api)
def cli():
    """Main entry point"""


@cli.command("init")
def init():
    """Init application, create database tables
    and create a new user named admin with password admin
    """
    from vf_api.extensions import db
    from vf_api.models import User
    click.echo("create database")
    db.create_all()
    click.echo("done")

    click.echo("create user")
    user = User(
        username='admin',
        email='admin@mail.com',
        password='admin',
        active=True
    )
    db.session.add(user)
    db.session.commit()
    click.echo("created user admin")

@cli.command("integration")
def integration():
    for test in integration_tests:
        test.__call__()


if __name__ == "__main__":
    cli()
