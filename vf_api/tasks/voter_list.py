import json

from flask.globals import current_app
import pandas as pd
import rq

from vf_api.extensions import rq as rq2
from vf_api.extensions import db
from vf_api.models import Task, VoterList, Voter
from vf_api.api.resources.voter_list.sqla_functions import (
    get_sqla_uri,
    get_sqla_engine
)


@rq2.job
def run_voter_list_query(user_id, voter_list_id):
    try:
        voter_list = db.session.query(VoterList).filter_by(id=voter_list_id).first()
    except Exception as e:
        print(e)
        print("List not found")
        voter_list_all = db.session.query(VoterList).all()
        print(voter_list_all)
        return "unable to run"
    if not voter_list:
        print("List not found")
        voter_list_all = db.session.query(VoterList).all()
        print(voter_list_all)
        return "404 error"
    if voter_list.query is not None:
        job = rq.get_current_job()
        if job:
            task = Task(
                id = job.id,
                name = 'Voter List Builder',
                description = 'Voter list {} building for user id {}'.format(voter_list_id, user_id),
                user_id = user_id
            )
            db.session.add(task)
        voter_list.status = 'Running'
        db.session.add(voter_list)
        db.session.commit()
        uri = get_sqla_uri()
        engine = get_sqla_engine(uri)
        voter_list_df = pd.read_sql(voter_list.query, engine)
        voter_list.voter_ids = []
        voter_ids = list(voter_list_df['voter_id'].unique())
        voter_objs = []
        for qvf_id in voter_ids:
            qid = str(qvf_id)
            voter = db.session.query(Voter).filter_by(qvf_number=qid).first()
            if not voter:
                voter = Voter(qvf_number=qid)
            voter_objs.append(voter)
        voter_list.voter_ids = voter_objs
        voter_list.status = 'Finished'
        voter_list.voter_list = voter_list_df.to_dict(orient="records")
        if job:
            task.complete = True
        db.session.add(voter_list)
        db.session.commit()
        if job:
            db.session.close()
        return "Job Finished"
        
