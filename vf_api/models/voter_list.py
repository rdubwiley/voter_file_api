from vf_api.extensions import db, pwd_context

voter_ids = db.Table(
    'voter_ids',
    db.Column('voter_list.id', db.Integer, db.ForeignKey('voter_list.id',onupdate="CASCADE", ondelete="CASCADE"), primary_key=True),
    db.Column('voter.id', db.Integer, db.ForeignKey('voter.id', onupdate="CASCADE", ondelete="CASCADE"), primary_key=True)
)


class VoterList(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    name = db.Column(db.Text)
    query = db.Column(db.Text)
    voter_list = db.Column(db.PickleType, nullable=True)
    status = db.Column(db.Text)
    voter_ids = db.relationship(
        'Voter', 
        secondary=voter_ids,
        lazy='subquery',
        passive_deletes=True,
        backref=db.backref('voter_lists', lazy=False),
        cascade='save-update, merge, delete'
    )


class Voter(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    qvf_number = db.Column(db.Text)