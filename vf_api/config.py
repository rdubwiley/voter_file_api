import os

"""Default configuration

Use env var to override
"""
DEBUG = False
SECRET_KEY = os.environ['VF_API_SECRET_KEY']

SQLALCHEMY_DATABASE_URI = os.environ["VF_API_DATABASE_URI"]
SQLALCHEMY_TRACK_MODIFICATIONS = False

VOTERFILE_DATABASE_URI = os.environ["VF_API_VOTERFILE_DATABASE_URI"]

JWT_BLACKLIST_ENABLED = True
JWT_BLACKLIST_TOKEN_CHECKS = ['access', 'refresh']
RQ_REDIS_URL = os.environ["VF_API_RQ_REDIS_URL"]

