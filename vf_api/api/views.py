from flask import Blueprint
from flask_restful import Api

from vf_api.api.resources import (
    TaskResource,
    TaskListResource,
    UserResource, 
    UserList,
    VoterSearchResource,
    VoterListCreationResource,
    VoterListBuildResource
)


blueprint = Blueprint('api', __name__, url_prefix='/api/v1')
api = Api(blueprint)


api.add_resource(UserResource, '/users/<int:user_id>')
api.add_resource(UserList, '/users')
api.add_resource(VoterSearchResource, '/voters/search')
api.add_resource(VoterListCreationResource, '/voters/list')
api.add_resource(VoterListBuildResource, '/voters/list/<int:voter_list_id>')
api.add_resource(TaskResource, '/task/<string:task_id>')
api.add_resource(TaskListResource, '/task/list')