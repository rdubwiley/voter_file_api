import json

from flask import request, jsonify
from flask import current_app as app
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from marshmallow import Schema, fields

from vf_api.extensions import ma, db
from vf_api.commons.pagination import paginate
from vf_api.models import Task, User, VoterList


class TaskSchema(ma.ModelSchema):

    class Meta:
        model = Task
        sqla_session = db.session


class TaskResource(Resource):

    method_decorators = [jwt_required]
    
    def get(self, task_id):
        schema = TaskSchema()
        task = Task.query.get_or_404(task_id)
        current_user = get_jwt_identity()
        if task.user_id != current_user:
            return "permission denied", 401
        return {"task": schema.dump(task).data}
    

class TaskListResource(Resource):
    
    method_decorators = [jwt_required]

    def get(self):
        schema = TaskSchema(many=True)
        current_user = get_jwt_identity()
        query = Task.query.filter_by(user_id = current_user)
        return paginate(query, schema)
