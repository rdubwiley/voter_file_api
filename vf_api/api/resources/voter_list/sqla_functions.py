import os

from flask import current_app
import numpy as np
import pandas as pd
from sqlalchemy import (
    create_engine, 
    MetaData, 
    select,
    and_,
    func,
    alias,
    between
)
from sqlalchemy.dialects import postgresql


def get_sqla_uri():
    return current_app.config['VOTERFILE_DATABASE_URI']


def get_sqla_engine(uri):
    return create_engine(uri)


def get_sqla_table(engine, table, schema=None):
    if schema:
        meta = MetaData(schema=schema)
        meta.reflect(bind=engine)
        return meta.tables['{}.{}'.format(schema,table)]
    else:
        meta = MetaData()
        meta.reflect(bind=engine) 
        return meta.tables[table]


def construct_voter_search_query(voter_table, search_conditions):
    stmt = select([voter_table]).where(and_(*search_conditions))
    return stmt.compile(compile_kwargs={"literal_binds": True},dialect=postgresql.dialect())


def get_election_code_query(election_table, election_type, year_start, year_end):
    query = select([election_table.c.election_code])\
        .where(and_(
            election_table.c.election_name == election_type,
            between(election_table.c.election_year, year_start, year_end)
        ))
    return query


def filter_voter_by_values(voter_table, value_dict):
    select_values = [ voter_table.c[key]==value_dict[key] for key in value_dict ]
    return select([voter_table]).where(and_(*select_values))


def filter_history_by_election_count(
    history_table, 
    election_table, 
    election_type, 
    min_num, 
    max_num,
    year_start,
    year_end,
    alias_name
):
    """Returns SQL query to get filtered table using an election count"""
    sub_query = get_election_code_query(
        election_table, 
        election_type,
        year_start,
        year_end
    )
    sub = alias(sub_query, alias_name)
    count = func.count(history_table.c.election_code).label('election_count')
    inner_query = history_table.join(
        sub,
        history_table.c.election_code == sub.c.election_code
    )
    stmt = select([history_table.c.voter_id, count])\
        .select_from(inner_query)\
        .group_by(history_table.c.voter_id)\
        .having(between(count, min_num,max_num))
    
    return stmt


def join_history_queries(history_queries, limit=None):
    union_queries = []
    for q_group in history_queries:
        join_query = q_group.pop()
        for i, h_query in enumerate(q_group):
            main = alias(join_query, 'h_main_{}'.format(i))
            sub = alias(h_query, 'h_join_{}'.format(i))
            join_stmt = main.join(sub, main.c.voter_id == sub.c.voter_id)
            join_query = select([main.c.voter_id]).select_from(join_stmt)
        union_queries.append(join_query)
    if len(union_queries) > 1:
        first_union = union_queries.pop()
        return first_union.union(*union_queries)
    else:
        return union_queries.pop()


def join_voter_queries(voter_queries):
    union_queries = []
    for q_group in voter_queries:
        join_query = q_group.pop()
        for i, v_query in enumerate(q_group):
            main = alias(join_query, 'v_main_{}'.format(i))
            sub = alias(v_query, 'v_join_{}'.format(i))
            join_stmt = main.join(sub, main.c.voter_id == sub.c.voter_id)
            join_query = select([main]).select_from(join_stmt)
        union_queries.append(join_query)
    if len(union_queries) > 1:
        first_union = union_queries.pop()
        return first_union.union(*union_queries)
    else:
        return union_queries.pop()


def construct_voter_list_query(
    voter_table,
    history_table,
    election_table,
    list_spec,
    limit = None
):
    if 'history_queries' in list_spec:
        h_filtered_queries = []
        for query in list_spec['history_queries']:
            h_filtered_queries.append([
                filter_history_by_election_count(
                    history_table,
                    election_table,
                    query['election_type'],
                    query['min_count'],
                    query['max_count'],
                    query['min_year'],
                    query['max_year'],
                    'alias_{}'.format(i)
                )
                for i, query in enumerate(query) 
            ])
        h_join_query = join_history_queries(h_filtered_queries, limit)
        h_join = alias(h_join_query, 'h_join')
        h_full_join = voter_table.join(
            h_join,
            voter_table.c.voter_id == h_join.c.voter_id
        )
    else:
        h_full_join = select([voter_table])
    h_stmt = select([voter_table.c.voter_id]).select_from(h_full_join)
    if 'file_queries' in list_spec:
        v_filtered_queries = []
        for query in list_spec['file_queries']:
            v_filtered_queries.append([
                filter_voter_by_values(voter_table, q)
                for i,q in enumerate(query)
            ])
        v_join_query = join_voter_queries(v_filtered_queries)
        v_join = alias(v_join_query, 'v_join')
        v_full_join = voter_table.join(
            v_join,
            voter_table.c.voter_id == v_join.c.voter_id
        )
        v_stmt = select([voter_table.c.voter_id]).select_from(v_full_join)
        final_join = alias(v_stmt, 'final_join')
        h = alias(h_stmt, 'h')
        final_full_join = final_join.join(
            h,
            final_join.c.voter_id == h.c.voter_id
        )
        stmt = select([final_join]).select_from(final_full_join)
    else:
        stmt = h_stmt
    if limit:
        stmt = stmt.limit(limit)
    return stmt.compile(compile_kwargs={"literal_binds": True},dialect=postgresql.dialect())