import json

from flask import request, jsonify, url_for
from flask import current_app as app
from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restful import Resource
from marshmallow import Schema, fields
import pandas as pd
import sqlalchemy as sa

from vf_api.extensions import ma, db, rq
from vf_api.commons.pagination import paginate
from vf_api.models import Task, User, VoterList
from vf_api.api.resources.voter_list.sqla_functions import (
    get_sqla_uri,
    get_sqla_engine,
    get_sqla_table,
    construct_voter_search_query,
    construct_voter_list_query
)
from vf_api.tasks.voter_list import run_voter_list_query 


class VoterSearchSchema(Schema):
    first_name = fields.Str()
    last_name = fields.Str()
    address = fields.Str()
    city = fields.Str()
    zipcode = fields.Integer()


class VoterQuerySchema(Schema):
    us_congress = fields.Integer()
    state_senate = fields.Integer()
    state_house = fields.Integer()
    county_code = fields.Str()
    city = fields.Str()
    zip = fields.Integer()
    ward_precinct = fields.Str()
    village_precinct = fields.Str()


class HistoryQuerySchema(Schema):
    election_type = fields.Str()
    vote_type = fields.Str()
    min_year = fields.Integer()
    max_year = fields.Integer()
    min_count = fields.Integer()
    max_count = fields.Integer()


class ListBuildingSchema(Schema):
    name = fields.Str(required=True)
    file_queries = fields.List(
        fields.Nested(VoterQuerySchema, many=True)
    )
    history_queries = fields.List(
        fields.Nested(HistoryQuerySchema, many=True)
    )


class VoterSearchResource(Resource):
    
    def post(self):
        schema = VoterSearchSchema()
        search, errors = schema.load(request.json)
        if errors:
            return errors, 422
        for key in search:
            try:
                search[key] = search[key].upper()
            except Exception:
                pass
        uri = get_sqla_uri()
        engine = get_sqla_engine(uri)
        voter_table = get_sqla_table(engine, 'voters', schema='voter_file')
        search_conditions = [ voter_table.c[key] == search[key] for key in search]
        query = construct_voter_search_query(voter_table, search_conditions)
        result = pd.read_sql(query, engine).to_dict(orient="records")
        return jsonify({"result": result})


class VoterListCreationResource(Resource):

    method_decorators = [jwt_required]
    
    def post(self):
        schema = ListBuildingSchema()
        current_user = get_jwt_identity()
        list_spec, errors = schema.load(request.json)
        if errors:
            return errors, 422
        uri = get_sqla_uri()
        engine = get_sqla_engine(uri)
        voter_table = get_sqla_table(engine, 'voters', schema='voter_file')
        history_table = get_sqla_table(engine, 'history', schema='voter_file')
        election_table = get_sqla_table(engine, 'elections', schema='voter_file')
        query = construct_voter_list_query(
            voter_table,
            history_table,
            election_table,
            list_spec
        )
        voter_list = VoterList(
            user_id = current_user,
            name = list_spec['name'],
            query = str(query),
            status = 'New'
        )
        db.session.add(voter_list)
        db.session.commit()
        return {"list_id": voter_list.id}, 201


class VoterListBuildResource(Resource):
    
    method_decorators = [jwt_required]

    def get(self, voter_list_id):
        current_user = get_jwt_identity()
        voter_list = db.session.query(VoterList).filter_by(id=voter_list_id).first()
        if voter_list.user_id != current_user:
            return "permission denied", 401
        if not voter_list.voter_list or voter_list.status == 'New':
            return {"msg": "You need to build your voter list"}
        if voter_list.status == 'Running':
            return {"msg": "Query still running"}
        page = int(request.args.get('page', 1))
        start_index = (page-1)*100
        end_index = (page*100)
        total_records = len(voter_list.voter_list)
        if start_index > 0:
            prev_url = url_for(
                request.endpoint,
                page=page-1,
                **request.view_args
            )
        else:
            prev_url = None
        if end_index < total_records:
            next_url = url_for(
                request.endpoint,
                page=page+1,
                **request.view_args
            )
        else:
            next_url = None
        return {
            "prev_url": prev_url,
            "next_url": next_url,
            "voter_list": voter_list.voter_list[start_index:end_index]
        }

    def post(self, voter_list_id):
        current_user = get_jwt_identity()
        voter_list = db.session.query(VoterList).filter_by(id=voter_list_id).first()
        if voter_list.user_id != current_user:
            return "permission denied", 401
        result = run_voter_list_query.queue(current_user, voter_list_id)
        return {"msg": "Voter file push started", "task_id": result.id}
    
    def put(self, voter_list_id):
        current_user = get_jwt_identity()
        voter_list = db.session.query(VoterList).filter_by(id=voter_list_id).first()
        if voter_list.user_id != current_user:
            return "permission denied", 401
        schema = ListBuildingSchema()
        list_spec, errors = schema.load(request.json)
        if 'name' in list_spec:
            voter_list.name = list_spec['name']
        if 'file_queries' in list_spec or 'history_queries' in list_spec:
            uri = get_sqla_uri()
            engine = get_sqla_engine(uri)
            voter_table = get_sqla_table(engine, 'voters', schema='voter_file')
            history_table = get_sqla_table(engine, 'history', schema='voter_file')
            election_table = get_sqla_table(engine, 'elections', schema='voter_file')
            query = construct_voter_list_query(
                voter_table,
                history_table,
                election_table,
                list_spec
            )
            voter_list.query = query
            voter_list.voter_list = None
            voter_list.status = 'New'
        db.session.commit()
        return f"voter list {voter_list_id} updated"