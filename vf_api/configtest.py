import os

SQLALCHEMY_DATABASE_URI = "sqlite:////tmp/testing_vf_api.db"

VOTERFILE_DATABASE_URI = os.environ["VF_API_VOTERFILE_DATABASE_URI"]

RQ_REDIS_URL = ["VF_API_RQ_REDIS_URL"]

