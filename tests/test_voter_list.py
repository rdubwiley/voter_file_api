import json
from time import sleep
from unittest.mock import patch

import mock
import pandas as pd
import pytest
from pytest_factoryboy import register
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import sqlparse

from vf_api.tasks.voter_list import run_voter_list_query
from vf_api.api.resources.voter_list.sqla_functions import (
    get_sqla_uri,
    get_sqla_engine,
    get_sqla_table,
    construct_voter_list_query
)
from vf_api.models import VoterList

@pytest.fixture
def voter_list(client, db, admin_user, admin_headers):
    vl = VoterList(
        user_id = admin_user.id,
        name = 'Test List',
        query = 'SELECT * FROM voter_file.voters LIMIT 105',
        status = 'New'
    )
    db.session.add(vl)
    db.session.commit()
    return vl

def test_search_voter(client, db, admin_headers):
    data = {
        'first_name': 'Ryan',
        'last_name': 'Wiley'
    }
    rep = client.post('api/v1/voters/search', json=data, headers=admin_headers)
    assert rep.status_code == 200

    rep_data = rep.get_json()['result']
    
    assert len(rep_data) > 0
    assert rep_data[0]['first_name'].title() == data['first_name'] 


def test_construct_voter_list_query(client, db):
    list_spec = {
        'history_queries': [
            [
                {
                    'election_type': 'STATE PRIMARY',
                    'min_count': 4,
                    'max_count': 4,
                    'min_year': 2010,
                    'max_year': 2016
                }
            ],
            [
                {
                    'election_type': 'STATE GENERAL',
                    'min_count': 4,
                    'max_count': 4,
                    'min_year': 2010,
                    'max_year': 2016
                }
            ]  
        ],
        'file_queries': [
            [
                {
                    'state_house': 1
                }
            ]
        ]
    }
    uri = get_sqla_uri()
    engine = get_sqla_engine(uri)
    voter_table = get_sqla_table(engine, 'voters', schema='voter_file')
    history_table = get_sqla_table(engine, 'history', schema='voter_file')
    election_table = get_sqla_table(engine, 'elections', schema='voter_file')
    vl_query = construct_voter_list_query(
        voter_table,
        history_table,
        election_table,
        list_spec,
        limit = 1
    )
    df = pd.read_sql(vl_query, engine)
    assert len(df) > 0

    list_spec = {}
    vl_query = construct_voter_list_query(
        voter_table,
        history_table,
        election_table,
        list_spec,
        limit = 1
    )


def test_create_voter_list(client, db, admin_user, admin_headers):

    data = {'name': 'test_voter_list'}
    rep = client.post(
        '/api/v1/voters/list',
        json=data,
        headers=admin_headers
    )
    assert rep.status_code == 201

    response_data = rep.get_json()
    voter_list_id = response_data['list_id'] 
    voter_list = db.session.query(VoterList).filter_by(id=voter_list_id).first()
    assert voter_list.id == voter_list_id 


def test_build_post_voter_list(
    client,
    voter_list
):
    rep = client.post(
        f'/api/v1/voters/list/{voter_list.id}'
    )
    assert rep.status_code != 200

   
def test_build_put_voter_list(
    client,
    db,
    admin_user,
    admin_headers,
    voter_list
):
    data = {'name': 'test put'}
    rep = client.put(
        f'/api/v1/voters/list/{voter_list.id}', 
        json=data,
        headers=admin_headers
    )
    assert rep.status_code == 200


def test_build_get_voter_list(
    client,
    db,
    admin_user,
    admin_headers,
    voter_list
):
    result = run_voter_list_query(voter_list.user_id, voter_list.id)
    rep = client.get(f'/api/v1/voters/list/{voter_list.id}', headers=admin_headers)
    assert rep.status_code == 200

    data = rep.get_json()
    assert len(data['voter_list']) == 100
    assert data['prev_url'] is None
    assert data['next_url'] is not None

    rep = client.get(data['next_url'], headers=admin_headers)
    assert rep.status_code == 200

    data = rep.get_json()
    assert len(data['voter_list']) == 5