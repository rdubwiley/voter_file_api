import json
from time import sleep
from unittest.mock import patch

import mock
import pandas as pd
import pytest
from pytest_factoryboy import register
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import sqlparse

from vf_api.models import Task


@pytest.fixture
def test_task(client, db, admin_user, admin_headers):
    task = Task(
        id = 'thisisatesttaskabcdef',
        user_id = admin_user.id,
        name = 'Test task',
        description = 'This is a test task'
    )
    db.session.add(task)
    db.session.commit()
    return task


def test_get_task(
    client, 
    db, 
    admin_user, 
    admin_headers, 
    test_task
):
    rep = client.get(f'/api/v1/task/{test_task.id}', headers=admin_headers)
    assert rep.status_code == 200
    
    data = rep.get_json()
    assert data['task']['name'] == test_task.name


def test_list_task(
    client, 
    db, 
    admin_user, 
    admin_headers, 
    test_task
):
    rep = client.get(f'/api/v1/task/list', headers=admin_headers)
    assert rep.status_code == 200
    
    data = rep.get_json()['results']
    assert data[0]['name'] == test_task.name