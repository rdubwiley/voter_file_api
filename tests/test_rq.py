from vf_api.tasks.example import dummy_task
from time import sleep

def test_rq():
    result = dummy_task.queue()
    for i in range(10):
        if not result.result:
            sleep(1)
    assert result.result == 'OK'
